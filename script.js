const gameContainer = document.getElementById("game");
const wonContainer = document.getElementById("won-container");
const startGameContainer = document.getElementById("start-game-container");

const startButton = document.getElementById("start-button");
const restartButton = document.getElementById("restart-button");
const headingDiv = document.getElementById("heading-div");

const header = document.getElementById("header");
const playAgainButton = document.getElementById("play-again-button");
const myScore = document.getElementById("my-score");

const wonScore = document.getElementById("won-score");
const bestScore = document.getElementById("best-score");

const card8 = document.getElementById("card-8");
const card12 = document.getElementById("card-12");
const card24 = document.getElementById("card-24");

const gameLine = document.getElementById("game-line");
const yesButton = document.getElementById("yes-button");
const noButton = document.getElementById("no-button");

const exitButton = document.getElementById("exit-button");
const exitAlertContainer = document.getElementById("exit-alert-container");
const exitYesButton = document.getElementById("exit-yes-button");
const exitNoButton = document.getElementById("exit-no-button");

const restartAlertContainer = document.getElementById(
  "restart-alert-container"
);

const allColors = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif",
];

// localStorage.removeItem("bestScore4");

let COLORS = [];

exitButton.addEventListener("click", function () {
  exitAlertContainer.classList.remove("no-display");
  gameContainer.style.pointerEvents = "none";
  restartButton.style.pointerEvents = "none";
});

exitYesButton.addEventListener("click", function () {
  window.location.reload();
  gameContainer.style.pointerEvents = "auto";
  restartButton.style.pointerEvents = "auto";
  exitAlertContainer.classList.add("no-display");
});

exitNoButton.addEventListener("click", function () {
  gameContainer.style.pointerEvents = "auto";
  restartButton.style.pointerEvents = "auto";
  exitAlertContainer.classList.add("no-display");
});

playAgainButton.addEventListener("click", function () {
  window.location.reload();
});

restartButton.addEventListener("click", function () {
  restartAlertContainer.classList.remove("no-display");
  gameContainer.style.pointerEvents = "none";
  exitButton.style.pointerEvents = "none";
});

yesButton.addEventListener("click", function () {
  restartAlertContainer.classList.add("no-display");
  gameContainer.style.pointerEvents = "auto";
  exitButton.style.pointerEvents = "auto";

  gameContainer.innerHTML = "";

  colorVisible = "";
  clickCount = 0;
  totalMatch = 0;
  addScore = 0;
  myScore.textContent = "My Score: 0";

  let shuffledColors = shuffle(COLORS);
  createDivsForColors(shuffledColors);
});

noButton.addEventListener("click", function () {
  gameContainer.style.pointerEvents = "auto";
  exitButton.style.pointerEvents = "auto";
  restartAlertContainer.classList.add("no-display");
});

let totalCardsTobeMatch;

function getColors(n) {
  let cards = allColors.splice(0, n);
  COLORS.push(...cards, ...cards);

  totalCardsTobeMatch = parseInt(COLORS.length / 2);

  getScore();

  startGameContainer.style.display = "none";
  header.classList.remove("no-display");

  let shuffledColors = shuffle(COLORS);
  createDivsForColors(shuffledColors);
}

card8.addEventListener("click", function (event) {
  getColors(4);
});

card12.addEventListener("click", function (event) {
  getColors(6);
});

card24.addEventListener("click", function (event) {
  getColors(12);
});

// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  console.log(array);
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

function getScore() {
  let getBestScore = localStorage.getItem(`bestScore${totalCardsTobeMatch}`);
  console.log(getBestScore === null);
  if (getBestScore === null || getBestScore === "") {
    console.log(getBestScore);
    localStorage.setItem(`bestScore${totalCardsTobeMatch}`, String(0));
    bestScore.textContent = `Best Score: 0`;
  } else {
    bestScore.textContent = `Best Score: ${getBestScore}`;
  }
}

function gameWon() {
  console.log("matched all");
  wonContainer.style.display = "flex";

  wonScore.textContent = `Your Score : ${addScore}`;

  let getBestScore = localStorage.getItem(`bestScore${totalCardsTobeMatch}`);

  if (getBestScore === "0") {
    localStorage.setItem(`bestScore${totalCardsTobeMatch}`, String(addScore));
    bestScore.textContent = `Best Score: ${Number(addScore)}`;
  } else {
    bestScoreValue = Number(getBestScore) > addScore ? addScore : getBestScore;
    gameLine.textContent =
      Number(getBestScore) > addScore
        ? "You defeted the best score"
        : "Try again, Improve your memory";
    localStorage.setItem(`bestScore${totalCardsTobeMatch}`, bestScoreValue);
    bestScore.textContent = `Best Score: ${Number(bestScoreValue)}`;
  }

  addScore = 0;
}

let colorVisible = "";
let clickCount = 0;

let totalMatch = 0;
let addScore = 0;

function handleCardClick(event) {
  console.log("you clicked", event.target);

  addScore++;
  myScore.textContent = `My Score: ${addScore}`;

  clickCount++;
  if (clickCount === 2) {
    if (
      colorVisible.getAttribute("class") === event.target.getAttribute("class")
    ) {
      console.log(colorVisible);
      console.log(event.target);

      colorVisible.removeEventListener("click", handleCardClick);
      event.target.removeEventListener("click", handleCardClick);

      colorVisible = "";
      clickCount = 0;
      totalMatch++;

      if (totalMatch === totalCardsTobeMatch) {
        console.log("matched all");
        setTimeout(() => {
          gameWon();
        });
      }
    } else {
      clickCount = 0;
      let children = gameContainer.querySelectorAll("div");
      console.log();
      gameContainer.style.pointerEvents = "none";
      setTimeout(() => {
        colorVisible.removeAttribute("style");
        event.target.removeAttribute("style");

        colorVisible.addEventListener("click", handleCardClick);
        event.target.addEventListener("click", handleCardClick);

        colorVisible = "";
        gameContainer.style.pointerEvents = "auto";
      }, 1000);
    }
  } else {
    colorVisible = event.target;
  }

  console.log(event.target.getAttribute("class"));
  let urlImage = event.target.getAttribute("class");
  event.target.style.backgroundImage = `url(${urlImage})`;
  event.target.style.backgroundSize = "cover";
  event.target.removeEventListener("click", handleCardClick);
}

// when the DOM loads

wonContainer.style.display = "none";
header.classList.add("no-display");
restartAlertContainer.classList.add("no-display");
exitAlertContainer.classList.add("no-display");
